<?php

namespace Megacoders\NewsModuleBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="news")
 * @UniqueEntity("slug")
 * @Gedmo\TranslationEntity(class="Translations\NewsItemTranslation")
 */
class NewsItem extends AbstractPersonalTranslatable implements TranslatableInterface
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @Assert\Regex("#^[a-z0-9-]+$#")
     * @var string
     */
    protected $slug;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\DateTime()
     * @var \DateTime
     */
    private $date;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable = true)
     * @var string
     */
    private $announce;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     * @var string
     */
    private $content;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    private $image;

    /**
     * @ORM\ManyToMany(targetEntity="NewsFeed")
     * @ORM\JoinTable(name="news_item__news_feed",
     *     joinColumns={@ORM\JoinColumn(name="news_item_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="news_feed_id", referencedColumnName="id")}
     * )
     * @var ArrayCollection|NewsFeed[]
     */
    private $feeds;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Megacoders\NewsModuleBundle\Entity\Translations\NewsItemTranslation",
     *     mappedBy="object",
     *     cascade={"persist", "remove"}
     * )
     * @var ArrayCollection
     */
    protected $translations;

    /**
     * NewsItem constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->feeds = new ArrayCollection();
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return NewsItem
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return NewsItem
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return NewsItem
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return NewsItem
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAnnounce()
    {
        return $this->announce;
    }

    /**
     * @param mixed $announce
     * @return NewsItem
     */
    public function setAnnounce($announce)
    {
        $this->announce = $announce;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return NewsItem
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return NewsItem
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return ArrayCollection|NewsFeed[]
     */
    public function getFeeds()
    {
        return $this->feeds;
    }

    /**
     * @param ArrayCollection|NewsFeed[] $feeds
     * @return NewsItem
     */
    public function setFeeds($feeds)
    {
        $this->feeds->clear();

        foreach ($feeds as $feed) {
            $this->addFeed($feed);
        }

        return $this;
    }

    /**
     * @param NewsFeed $feed
     * @return $this
     */
    public function addFeed(NewsFeed $feed) {
        if (!$this->feeds->contains($feed)) {
            $this->feeds->add($feed);
        }

        return $this;
    }

    /**
     * @param NewsFeed $feed
     * @return $this
     */
    public function removeFeed(NewsFeed $feed) {
        $this->feeds->removeElement($feed);
        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getTitle();
    }

}
