<?php

namespace Megacoders\NewsModuleBundle\Entity\Translations;

use Doctrine\ORM\Mapping as ORM;
use Megacoders\NewsModuleBundle\Entity\NewsItem;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslation;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="news_item_translation",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_news_item_translation_idx", columns={
 *         "locale", "object_id", "field"
 *     })}
 * )
 */
class NewsItemTranslation extends AbstractPersonalTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="Megacoders\NewsModuleBundle\Entity\NewsItem", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
     * @var NewsItem
     */
    protected $object;

}
