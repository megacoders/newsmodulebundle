<?php

namespace Megacoders\NewsModuleBundle\Entity\Translations;

use Doctrine\ORM\Mapping as ORM;
use Megacoders\NewsModuleBundle\Entity\NewsFeed;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslation;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="news_feed_translation",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_news_feed_translation_idx", columns={
 *         "locale", "object_id", "field"
 *     })}
 * )
 */
class NewsFeedTranslation extends AbstractPersonalTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="Megacoders\NewsModuleBundle\Entity\NewsFeed", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
     * @var NewsFeed
     */
    protected $object;

}
