<?php

namespace Megacoders\NewsModuleBundle\Admin;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Megacoders\AdminBundle\Admin\BaseAdmin;
use Megacoders\AdminBundle\Form\Type\UploadedImageType;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class NewsItemAdmin extends BaseAdmin
{

    /**
     * @var string
     */
    protected $baseRoutePattern = 'content/news';

    /**
     * @var array
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'date',
    );

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper
            ->add('title', null, ['label' => 'admin.entities.news_item.title'])
            ->add('date',
                null, ['label' => 'admin.entities.news_item.date'],
                'sonata_type_datetime_picker', ['format' => 'yyyy-MM-dd HH:mm'])
            ->add('feeds',
                null, ['label' => 'admin.entities.news_item.feeds'],
                null, ['multiple' => true]);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title', null, ['label' => 'admin.entities.news_item.title'])
            ->add('date', null, [
                'label' => 'admin.entities.news_item.date',
                'pattern' => 'yyyy-MM-dd HH:mm'
            ])
            ->add('_action', null, [
                'label' => 'admin.actions._actions',
                'actions' => ['edit' => [], 'delete' => []]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $formMapper
            ->with('admin.labels.news_item')
                ->add('title', null, ['label' => 'admin.entities.news_item.title'])
                ->add('slug', null, ['label' => 'admin.entities.news_item.slug'])
                ->add('date', 'sonata_type_datetime_picker', [
                    'label' => 'admin.entities.news_item.date',
                    'format' => 'yyyy-MM-dd HH:mm'
                ])
                ->add('announce', CKEditorType::class, [
                    'label' => 'admin.entities.news_item.announce',
                    'required' => false
                ])
                ->add('content', CKEditorType::class, ['label' => 'admin.entities.news_item.content'])
                ->add('image', UploadedImageType::class, [
                    'label' => 'admin.entities.banner.image',
                    'required' => false,
                    'upload_directory' => $container->getParameter('megacoders_news.upload_directory'),
                    'public_directory' => $container->getParameter('megacoders_news.public_directory')
                ])
                ->add('feeds', null, ['label' => 'admin.entities.news_item.feeds'])
            ->end();
    }

}
