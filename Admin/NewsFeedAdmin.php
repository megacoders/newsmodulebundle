<?php

namespace Megacoders\NewsModuleBundle\Admin;

use Megacoders\AdminBundle\Admin\BaseAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class NewsFeedAdmin extends BaseAdmin
{
    /**
     * @var string
     */
    protected $baseRoutePattern = 'content/news-feeds';

    /**
     * @var array
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'name',
    );

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, ['label' => 'admin.entities.news_feed.name'])
            ->add('title', null, ['label' => 'admin.entities.news_feed.title'])
            ->add('_action', null, [
                'label' => 'admin.actions._actions',
                'actions' => ['edit' => [], 'delete' => []]
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('admin.labels.news_feed')
                ->add('name', null, ['label' => 'admin.entities.news_feed.name'])
                ->add('title', null, ['label' => 'admin.entities.news_feed.title'])
            ->end()
        ;
    }
}
