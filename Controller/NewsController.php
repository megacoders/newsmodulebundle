<?php
namespace Megacoders\NewsModuleBundle\Controller;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Gedmo\Translatable\Query\TreeWalker\TranslationWalker;
use Megacoders\ListModuleBundle\Controller\AbstractListController;
use Megacoders\MenuModuleBundle\Model\BreadcrumbMenuItem;
use Megacoders\NewsModuleBundle\Entity\NewsFeed;
use Megacoders\NewsModuleBundle\Entity\NewsItem;
use Megacoders\PageBundle\Entity\Meta;
use Megacoders\PageBundle\Entity\PageBlock;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class NewsController extends AbstractListController
{

    /**
     * @var NewsFeed[]
     */
    private $feeds = null;

    /**
     * {@inheritdoc}
     */
    public function configureRoutes($baseName, $baseUrl, PageBlock $block)
    {
        $collection = new RouteCollection();

        /** List of objects */
        $collection->add(
            $baseName .'_list',
            new Route($baseUrl .'/{page}', ['page' => 1], ['page' => '\d+'])
        );

        /** Object details */
        $collection->add(
            $baseName .'_details',
            new Route($baseUrl .'/details/{slug}', ['_action' => 'details'], ['slug' => '^[a-z0-9-]+$'])
        );

        return $collection;
    }

    /**
     * @return NewsFeed[]
     */
    public function getFeeds()
    {
        if ($this->feeds !== null) {
            return $this->feeds;
        }

        /** @var EntityRepository $repository */
        $repository = $this->getDoctrine()->getRepository(NewsFeed::class);
        $queryBuilder = $repository->createQueryBuilder('nf', 'nf.id');

        $queryBuilder
            ->andWhere('nf.id in (:ids)')
            ->setParameter('ids', $this->getModuleParameter('feeds'));

        $this->feeds = $queryBuilder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getResult();

        return $this->feeds;
    }

    /**
     * {@inheritdoc}
     */
    protected  function getObjectRequestKey()
    {
        return 'slug';
    }

    /**
     * {@inheritdoc}
     */
    protected function getQueryBuilder($limit = null, $offset = null)
    {
        /** @var EntityRepository $repository */
        $repository = $this->getDoctrine()->getRepository(NewsItem::class);
        $queryBuilder = $repository->createQueryBuilder('n', 'n.id');

        $queryBuilder
            ->select('n, nf')
            ->leftJoin('n.feeds', 'nf')
            ->andWhere('nf.id in (:feeds)')
            ->setParameter('feeds', $this->getModuleParameter('feeds'))
            ->orderBy('n.date', Criteria::DESC);

        if ($limit > 0) {
            $queryBuilder->setMaxResults($limit);
        }

        if ($offset > 0) {
            $queryBuilder->setFirstResult($offset);
        }

        return $queryBuilder;
    }

    /**
     * {@inheritdoc}
     */
    protected function getDetailsQueryBuilder()
    {
        return $this->getQueryBuilder();
    }

    /**
     * @param NewsItem $newsItem
     */
    protected function addPageExtraBreadcrumb($newsItem)
    {
        $breadcrumbMenuItem = new BreadcrumbMenuItem($newsItem->getTitle(), $this->generateDetailsUrl($newsItem));
        $breadcrumbMenuItem->setActive(true);

        $this->getPage()->setExtra('breadcrumb', $breadcrumbMenuItem);
    }

    /**
     * @param NewsItem $newsItem
     */
    protected function addPageMetaTitle($newsItem)
    {
        $metaTitle = new Meta();
        $metaTitle
            ->setName('title')
            ->setContent($newsItem->getTitle())
        ;

        $this->getPage()->setMeta($metaTitle);
    }

}
