<?php

namespace Megacoders\NewsModuleBundle\EventListener;


use Doctrine\ORM\Event\LifecycleEventArgs;
use Megacoders\NewsModuleBundle\Entity\NewsItem;
use Symfony\Component\DependencyInjection\ContainerInterface;

class NewsDoctrineListener
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * NewsDoctrineListener constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postRemove(LifecycleEventArgs $args)
    {
        $object = $args->getEntity();

        if ($object instanceof NewsItem) {
            $this->removeNewsItemImageFileByPublicPath($object->getImage());
        }
    }

    /**
     * @param string $publicPath
     */
    private function removeNewsItemImageFileByPublicPath($publicPath)
    {
        $uploadDirectory = $this->container->getParameter('megacoders_news.upload_directory');

        if (!$publicPath) {
            return;
        }

        $fileInfo = pathinfo($publicPath);
        $uploadPath = $uploadDirectory .'/' .$fileInfo['basename'];

        if (is_file($uploadPath)) {
            unlink($uploadPath);
        }
    }
}
